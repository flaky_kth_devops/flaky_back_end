from flask import Flask, request, jsonify
from flask_cors import CORS
import configparser
import os

""" Flask application """
app = Flask(__name__)
CORS(app, resources={r"*": {"origins": "https://flaky.jesperlarsson.me"}})

build_failing = False
artwork = True
gitlab = True

"""
No default behaviour. Should have index.html for this. 
TODO : make sure the above is the case
"""


@app.route("/")
def default():
    return "Hello"


@app.route("/status", methods=['GET'])
def status_of_build():
    return jsonify(build_failing)


@app.route("/mode", methods=['GET'])
def configuration():
    return jsonify(artwork)


@app.route("/flip-mode", methods = ['POST'])
def flip_mode():
    global artwork
    artwork = not artwork
    return jsonify(artwork)

    
@app.route("/hook", methods = ['POST'])
def build_failure():
    """
    If a build fails, make sure that the heartbeat reacts accordingly.
    """

    if not request.is_json:
        return None

    data = request.get_json()
    update_status(data)

    return jsonify(build_failing)


def update_status(json):
    """
    Update variable build_failing depending on the event
    """
    global gitlab
    
    if gitlab:
        gitlab_pipeline_status(json)


def gitlab_pipeline_status(json):
    global build_failing

    if json.get("object_attributes").get("status") == "success":
        build_failing = False
    else:
        build_failing = True


def main():
    global artwork

    """
    Load the config and start the server
    """
    config = configparser.ConfigParser()
    config.read("heartbeat.ini")
    mode = config['Heartbeat']['mode']
    if "status" in mode:
        artwork = False


if __name__ == '__main__':
    main()
    app.run(host="0.0.0.0", port=os.environ['PORT'])
